from sys import argv

from sniff import block

def main(argv):
    def usage():
        print("syntax : python3 tcp_block.py <interface> <host>")
        print("sample : python3 tcp_block.py ens33 test.gilgil.net")

    try:
        name, interface, host=argv
    except:
        usage()
        exit()

    block(interface, host)

if __name__ == '__main__':
    argv='tcp_block.py ens33 test.gilgil.net'.split()
    main(argv)