from struct import pack, unpack_from

from conversion import *

broadcast_mac = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff]
unknown_mac   = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00]

HTTP_OPS = 'GET POST HEAD PUT DELETE OPTIONS'.split(' ')

def checksum(pkt):
    chk=0
    if len(pkt)%2:
        pkt+=b'\x00'
    
    for i in range(0, len(pkt), 2):
        chk+=(pkt[i]<<8) + (pkt[i+1])

    while chk!=(chk&0xffff):
        chk=(chk>>16)+(chk&0xffff)

    chk=~chk&0xffff

    return chk

class Packet():
    @classmethod
    def analyze(cls, packet):
        #input: packet, a bytes object
        #output: cls object, a disassembled packet in its object form.
        raise NotImplementedError("analyze must be implemented by its subclasses")

    def get_packet(self, **kwargs):
        #returns a bytes object, a packet to be transmitted.
        raise NotImplementedError("get_packet must be implemented by its subclasses")

    def __len__(self):
        return len(self.get_packet())

class ETH_packet(Packet):
    '''
    !   : Network byte order
    6B  : Destination MAC
    6B  : Source MAC
    H   : Ethertype(ETY)
    '''
    ETHER_FRAME='!6B6BH'

    @classmethod
    def analyze(cls, packet):
        eth_header=unpack_from(cls.ETHER_FRAME, packet)
        self=ETH_packet()

        self.dst_mac=[*(eth_header[0:6])]
        self.src_mac=[*(eth_header[6:12])]

        self.ethertype=eth_header[12]

        self.payload=packet[14:]

        return self

    def __str__(self):
        return 'src MAC: {}\ndst MAC: {}\nethtype: {:04X}\n===PAYLOAD===\n{}'.format(arr_to_mac(self.src_mac), arr_to_mac(self.dst_mac), self.ethertype, self.payload)

    def get_packet(self, **kwargs):
        if isinstance(self.payload, Packet):
            return pack(self.ETHER_FRAME, *self.dst_mac, *self.src_mac, self.ethertype)+self.payload.get_packet(**kwargs)
        return pack(self.ETHER_FRAME, *self.dst_mac, *self.src_mac, self.ethertype)+self.payload

class ARP_packet(Packet):
    '''
    !   : Network byte order
    H   : Hardware Type(HRD)
    H   : Protocol Type(PRO)
    B   : Hardware Addr len(HLN)
    B   : Protocol Addr len(PLN)
    H   : Opcode(1 for request, 2 for response)
    6B  : Sender MAC addr
    4B  : Sender IP addr
    6B  : Target MAC addr
    4B  : Target IP addr
    '''
    ARP_FRAME='!HHBBH6B4B6B4B'
    HRD_BASE=0x0001
    PRO_BASE=0x0800
    HLN_BASE=0x06
    PLN_BASE=0x04

    ETHERTYPE=0x0806

    ARP_REQUEST=0x0001
    ARP_REPLY=0x0002

    @classmethod
    def analyze(cls, packet):
        if isinstance(packet, Packet):
            return packet

        arp_packet=unpack_from(cls.ARP_FRAME, packet)
        self=ARP_packet()
        self.HRD=arp_packet[0]
        self.PRO=arp_packet[1]
        self.HLN=arp_packet[2]
        self.PLN=arp_packet[3]
        self.opcode=arp_packet[4]
        self.sender_mac=arp_packet[5:11]
        self.sender_ip=arp_packet[11:15]
        self.target_mac=arp_packet[15:21]
        self.target_ip=arp_packet[21:25]
        return self

    def __str__(self):
        return 'opcode : {}\nsnd MAC: {}\nsnd IP : {}\ntgt MAC: {} \ntgt IP : {}'.format(self.opcode, arr_to_mac(self.sender_mac), arr_to_ip(self.sender_ip), arr_to_mac(self.target_mac), arr_to_ip(self.target_ip))

    def get_packet(self, *, pad_len=18):
        return pack(self.ARP_FRAME, self.HRD, self.PRO, self.HLN, self.PLN, self.opcode,
                    *self.sender_mac, *self.sender_ip, *self.target_mac, *self.target_ip)+b'\x00'*pad_len#zero padding

class IPv4_packet(Packet):
    '''
    !   : Network Byte Order
    B   : Version, IP Header Length
    B   : Type of Service(DSCP, ECN)
    H   : Total Length
    H   : ID
    H   : Flag, Fragment Offset
    B   : TTL
    B   : protocol
    H   : Header Checksum
    4B  : Source IP
    4B  : Destination IP
    '''
    IP_FRAME='!BBHHHBBH4B4B'
    ETHERTYPE=0x0800
    IPV4=4

    @classmethod
    def analyze(cls, packet):
        if isinstance(packet, Packet):
            return packet
        
        ip_packet=unpack_from(cls.IP_FRAME, packet)
        self=IPv4_packet()
        self.ver, self.ip_hl=(ip_packet[0]&0xf0)>>4, (ip_packet[0]&0x0f)
        self.DSCP_ECN=ip_packet[1]
        self.totlen=ip_packet[2]
        self.id=ip_packet[3]
        self.flg_fragoff=ip_packet[4]
        self.TTL=ip_packet[5]
        self.protocol=ip_packet[6]
        self.checksum=ip_packet[7]
        self.src_ip=ip_packet[8:12]
        self.dst_ip=ip_packet[12:16]

        self.options=packet[20:self.ip_hl*4]
        self.payload=packet[self.ip_hl*4:]
        
        return self

    def __str__(self):
        return 'src IP: {}\ndst IP: {}\n===PAYLOAD===\n{}'.format(arr_to_ip(self.src_ip), arr_to_ip(self.dst_ip), self.payload)

    def update(self):
        self.checksum=0
        self.payload=b''
        pkt=self.get_packet(renew=False)
        self.checksum=checksum(pkt[:self.ip_hl*4])
        self.totlen=len(pkt)
        return self.checksum

    def get_packet(self, *, renew=True):
        if renew:
            self.update()
        base=pack(self.IP_FRAME, (self.ver<<4)^self.ip_hl, self.DSCP_ECN, self.totlen, self.id, self.flg_fragoff,
            self.TTL, self.protocol, self.checksum, *self.src_ip, *self.dst_ip)+self.options
        if isinstance(self.payload, Packet):
            return base+self.payload.get_packet(renew=renew)
        else:
            return base+self.payload

class TCP_packet(Packet):
    '''
    !   : Network Byte Order
    H   : Source port
    H   : Destination port
    L   : Sequence number
    L   : Acknowledgement number
    H   : Data offset, reserved, flags
    H   : Window size
    H   : Checksum
    H   : Urgent pointer
    '''
    TCP_FRAME='!HHLLHHHH'
    '''
    4B  : Source address
    4B  : Destination address
    B  : Zeroes
    B   : Protocol(IP_PROTOCOL=0x06)
    H   : TCP length(=len(packet))
    '''
    PSEUDOHEADER_FRAME='!4B4BBBH'
    IP_PROTOCOL=0x06

    @classmethod
    def analyze(cls, packet, src_addr, dst_addr):
        if isinstance(packet, Packet):
            return packet

        tcp_packet=unpack_from(cls.TCP_FRAME, packet)
        self=TCP_packet()
        self.src_addr=src_addr
        self.dst_addr=dst_addr

        self.src_port=tcp_packet[0]
        self.dst_port=tcp_packet[1]
        self.seq_num=tcp_packet[2]
        self.ack_num=tcp_packet[3]
        self.data_offset=(tcp_packet[4]&0xf000)>>12
        self.flags=tcp_packet[4]&0x0fff
        self.window_size=tcp_packet[5]
        self.checksum=tcp_packet[6]
        self.urgent=tcp_packet[7]
        self.options=packet[20:self.data_offset*4]
        self.data=packet[self.data_offset*4:]
        return self

    def __str__(self):
        return 'src port: {}\ndst port: {}\n===  DATA  ===\n{}'.format(self.src_port, self.dst_port, self.data)

    @property
    def ack(self):
        return (self.flags&0b_0000_0001_0000)>>4

    @ack.setter
    def ack(self, val:bool):
        return self.flags&0b1111_1110_1111^(0b_0000_0001_0000*val)

    @property
    def psh(self):
        return (self.flags&0b0000_0000_1000)>>2

    @psh.setter
    def psh(self, val:bool):
        return self.flags&0b1111_1111_0111^(0b0000_0000_1000*val)

    @property
    def rst(self):
        return (self.flags&0b0000_0000_0100)>>2

    @rst.setter
    def rst(self, val:bool):
        return self.flags&0b1111_1111_1011^(0b0000_0000_0100*val)

    @property
    def syn(self):
        return (self.flags&0b0000_0000_0010)>>1

    @syn.setter
    def syn(self, val:bool):
        return self.flags&0b1111_1111_1101^(0b0000_0000_0010*val)

    @property
    def fin(self):
        return self.flags&0b0000_0000_0001

    @fin.setter
    def fin(self, val:bool):
        return self.flags&0b1111_1111_1110^(0b0000_0000_0001*val)

    def get_pseudoheader(self):
        return pack(self.PSEUDOHEADER_FRAME, *self.src_addr, *self.dst_addr, 0, self.IP_PROTOCOL, len(self.get_packet(renew=False)))

    def update(self):
        #checksum
        self.checksum=0xffff
        pkt=self.get_pseudoheader()+self.get_packet(renew=False)#[:self.data_offset*4]
        self.checksum=checksum(pkt)
        return self.checksum

    def get_packet(self, *, renew=True):
        if renew:
            self.update()
        base=pack(self.TCP_FRAME, self.src_port, self.dst_port, self.seq_num, self.ack_num, (self.data_offset<<12)^self.flags,
                    self.window_size, self.checksum, self.urgent)+self.options
        if isinstance(self.data, Packet):
            return base+self.data.get_packet(renew=renew)
        else:
            return base+self.data

class HTTP_packet(Packet):
    @classmethod
    def analyze(cls, packet):
        if not HTTP_packet.is_http(packet):
            raise Exception("Given packet is not HTTP")
        self=HTTP_packet()

        self.data=packet

        return self

    def __str__(self):
        return self.plain

    @property
    def data(self):
        return self._data

    @property
    def plain(self):
        return self._plain

    @property
    def host(self):
        try:
            return self._host
        except:
            start=self.plain.find('Host: ')
            if start==-1:
                self._host=None
            else:
                end=self.plain.find('\r\n', start)
                self._host=self.plain[start+len('Host: '):end]
            return self._host

    @host.setter
    def host(self, something):
        raise Exception

    @data.setter
    def data(self, packet):
        self._data=packet
        self._plain=packet.decode().lstrip()

    @plain.setter
    def plain(self, http):
        self._plain=http
        self._data=http.encode()

    @classmethod
    def is_http(cls, packet):
        if not isinstance(packet, str):
            try:
                packet=packet.decode().lstrip()
            except:
                return False
        for op in HTTP_OPS:
            if packet.startswith(op):
                return True
        return False

    def get_packet(self, *, renew=True):
        return self._data

def analyze_packet(packet):
    eth=ETH_packet.analyze(packet)
    if eth.ethertype==ARP_packet.ETHERTYPE:
        arp=eth.payload=ARP_packet.analyze(eth.payload)
    elif eth.ethertype==IPv4_packet.ETHERTYPE:
        ip=eth.payload=IPv4_packet.analyze(eth.payload)
        if ip.protocol==TCP_packet.IP_PROTOCOL:
            tcp=ip.payload=TCP_packet.analyze(ip.payload, ip.src_ip, ip.dst_ip)
            if HTTP_packet.is_http(tcp.data):
                http=tcp.data=HTTP_packet.analyze(tcp.data)
    return eth

if __name__ == '__main__':
    packet="\x00\x50\x56\xef\x4f\xc4\x00\x0c\x29\xfb\xa8\xd8\x08\x00\x45\x00\x00\x28\x55\xcb\x40\x00\x40\x06\x0d\xde\xc0\xa8\x43\x82\xaf\xd5\x23\x27\xbc\x38\x00\x50\x5a\x32\xb2\xa7\x64\x8b\x5f\x17\x50\x10\xfa\xf0\xd7\x41\x00\x00"
    
    eth=analyze_packet(packet.encode())

    fix=lambda x:'0'*(2-len(hex(ord(x) if type(x) in [str, bytes] else x)[2:]))+hex(ord(x) if type(x) in [str, bytes] else x)[2:]

    print(*map(fix,unpack_from(ETH_packet.ETHER_FRAME, packet.encode())))

    print(*map(fix, eth.dst_mac))

    print(*map(fix, eth.src_mac))

    for _ in packet:
        print(fix(_), end=' ')
    print()

    for _ in eth.get_packet(renew=True):
        print(fix(_), end=' ')
    print()

    print(eth.get_packet(renew=True)==packet)