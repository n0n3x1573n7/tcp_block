from copy import deepcopy as dcopy
import socket

from packet import *

PRESET_FRWD=b''
PRESET_BACK=b''
'HTTP/1.1 404 NOT FOUND\r\n'

def get_forward_packet(*, packet=None, eth=None):
    if isinstance(eth, ETH_packet):
        eth=dcopy(eth)
    else:
        eth=analyze_packet(packet)
    ip=eth.payload
    tcp=ip.payload
    http=tcp.data

    eth.dst_mac,  eth.src_mac  = eth.dst_mac,  eth.src_mac
    ip.src_ip,    ip.dst_ip    = ip.src_ip,    ip.dst_ip
    tcp.src_port, tcp.dst_port = tcp.src_port, tcp.dst_port

    new_seq=tcp.seq_num+len(http)
    tcp.seq_num, tcp.ack_num = new_seq, tcp.ack_num
    tcp.ack=False
    tcp.psh=False
    tcp.rst=True
    tcp.data=PRESET_FRWD

    return eth.get_packet()

def get_backward_packet(*, packet=None, eth=None):
    if isinstance(eth, ETH_packet):
        eth=dcopy(eth)
    else:
        eth=analyze_packet(packet)

    ip=eth.payload
    tcp=ip.payload
    http=tcp.data

    eth.dst_mac,  eth.src_mac  = eth.src_mac,  eth.dst_mac
    ip.src_ip,    ip.dst_ip    = ip.dst_ip,    ip.src_ip
    tcp.src_port, tcp.dst_port = tcp.dst_port, tcp.src_port

    new_seq=tcp.seq_num+len(http)
    tcp.seq_num, tcp.ack_num = tcp.ack_num, new_seq
    tcp.ack=False
    tcp.psh=False
    tcp.rst=True
    tcp.data=PRESET_BACK

    return eth.get_packet()

def block(interface, host):
    sock=socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0003))
    sock.bind((interface, 0))
    
    while 1:
        packet, addr=sock.recvfrom(65535+30)
        eth=analyze_packet(packet)
        try:
            ip=eth.payload
            tcp=ip.payload
            http=tcp.data
            print(packet)
            print(eth.get_packet(renew=False))
            print()
            print(http.host, http.host==host)
            if http.host==host:
                sock.send(get_backward_packet(eth=eth))
                sock.send(get_forward_packet(eth=eth))
                print(host, 'blocked')
        except Exception as e:
            if 'object has no attribute' not in str(e):
                print(e)